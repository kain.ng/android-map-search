package com.example.myfirstmap;

import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts.RequestPermission;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentActivity;

import android.Manifest;
import android.app.DownloadManager;
import android.app.TabActivity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.location.Location;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.SearchView;
import android.widget.TabHost;

import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.AutocompletePrediction;
import com.google.android.libraries.places.api.model.AutocompleteSessionToken;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.api.model.RectangularBounds;
import com.google.android.libraries.places.api.net.FetchPlaceRequest;
import com.google.android.libraries.places.api.net.FetchPlaceResponse;
import com.google.android.libraries.places.api.net.FindAutocompletePredictionsRequest;
import com.google.android.libraries.places.api.net.FindAutocompletePredictionsResponse;
import com.google.android.libraries.places.api.net.PlacesClient;
import com.google.android.libraries.places.widget.AutocompleteSupportFragment;
import com.google.android.libraries.places.widget.listener.PlaceSelectionListener;
import com.google.android.material.tabs.TabLayout;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback, OnSuccessListener,  ActivityResultCallback<Boolean>, OnFailureListener, TabLayout.OnTabSelectedListener {
    private final String TAG = "-- MapsActivity -- ";
    private SearchView mSearchView;
    private Location location;
    private ActivityResultLauncher<String> requestPermissionLauncher;
    private FusedLocationProviderClient fusedLocationClient;
    private AutocompleteSessionToken token;
    private RectangularBounds bounds;
    private FindAutocompletePredictionsRequest predictionRequest;
    private FetchPlaceRequest fetchPlaceRequest;
    private String searchQuery;

    private GoogleMap mMap;
    private PlacesClient placesClient;
    public static ArrayList<Place> mLocations;
    public static ArrayList<String> mExtraIDs;

    private TabLayout tabLayout;

    public void onTabReselected(TabLayout.Tab tab) {
        Log.i(TAG, "same tab has been re-selected, doing nothing");
    }

    public void onTabSelected(TabLayout.Tab tab) {
        Log.i(TAG, "a tab has been selected");
        if (tab == tabLayout.getTabAt(1)) {
            Intent intent = new Intent(this, ResultsActivity.class);
            //create each extra id(string) as the index of mLocations

//            for (int i = 0; i < mLocations.size() -1; i++) {
//                intent.putExtra(Integer.toString(i), mLocations.get(i));
//            }
            intent.putExtra("locations", mLocations);
            startActivity(intent);
        }
    }

    public void onTabUnselected(TabLayout.Tab tab) {

    }

    public void onSubmit(View view) {
        EditText editText = (EditText)findViewById(R.id.searchText);
        searchQuery = editText.getText().toString();

        // ----------- clear prev search result markers
        mMap.clear();

        // ----------- get places around target location
        token = AutocompleteSessionToken.newInstance();
        bounds = RectangularBounds.newInstance(new LatLng(location.getLatitude()-0.01, location.getLongitude()-0.01), new LatLng(location.getLatitude()+0.01, location.getLongitude()+0.01));
        predictionRequest = FindAutocompletePredictionsRequest.builder()
                .setLocationBias(bounds)
                .setOrigin(new LatLng(location.getLatitude(), location.getLongitude()))
                .setSessionToken(token)
                .setQuery(searchQuery).build();

        placesClient.findAutocompletePredictions(predictionRequest).addOnSuccessListener(this).addOnFailureListener(this);
    }

    @Override
    protected void onResume() {
        Log.i(TAG, "maps activity OnResume()");
        super.onResume();
        tabLayout.getTabAt(0).select();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);

        tabLayout = (TabLayout) findViewById(R.id.tablayout);
        tabLayout.addOnTabSelectedListener(this);

        requestPermissionLauncher = registerForActivityResult(new RequestPermission(), this);
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this);

        Places.initialize(getApplicationContext(), getString(R.string.google_maps_key));
        placesClient = Places.createClient(this);

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        mLocations = new ArrayList<Place>();
        mExtraIDs = new ArrayList<String>();
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        Log.i(TAG, "Requesting Permission now");
        requestPermissionLauncher.launch(Manifest.permission.ACCESS_FINE_LOCATION);
    }

    // Finished requesting permission
    public void onActivityResult(Boolean isGranted) {
        Log.i(TAG, "please wait... checking if permission was granted");

        if (isGranted) {
            Log.i(TAG, "Successfully got permission");
        } else {
            Log.e(TAG, "User denied permission. Will not center map on user's location");
            return;
        }

        if(ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            Log.e(TAG,"Should not happen! User has granted permission, but somehow we still don't have permission.");
            return;
        }

        fusedLocationClient.getLastLocation().addOnSuccessListener(this, this);
    }

    // implements OnSuccessListener
    public void onSuccess(Object object) {
        Log.i(TAG, "onsuccess called for object");
        if (object instanceof Location) {
            Log.i(TAG, "Detected a location object");
            onSuccess((Location)object);
            return;
        }
        if (object instanceof FindAutocompletePredictionsResponse) {
            Log.i(TAG, "Detected a autocomplete object");
            onSuccess((FindAutocompletePredictionsResponse)object);
            return;
        }
        if (object instanceof FetchPlaceResponse) {
            Log.i(TAG, "Detected a FetchPlaceResponse");
            onSuccess((FetchPlaceResponse)object);
            return;
        }

        Log.e(TAG, "Should not happen! Received unknown onSuccess object");
    }

    // Finished getting user's location.
    private void onSuccess(Location location) {
        this.location = location;
        Log.i(TAG, "onsuccess called for Loation");
        // Got last known location. In some rare situations this can be null.
        if (location == null) {
            Log.e(TAG, "Location == null!");
        }

        LatLng lastLocation = new LatLng(location.getLatitude(), location.getLongitude());

        // Logic to handle location object
        mMap.addMarker(new MarkerOptions().position(lastLocation).title("your location"));
        CameraUpdate cm = CameraUpdateFactory.newLatLngZoom(lastLocation, 10);
        mMap.moveCamera(cm);
    }

    // finished getting autocomplete predictions
    private void onSuccess(FindAutocompletePredictionsResponse response) {
        String predictedPlaceId;
        for(AutocompletePrediction prediction : response.getAutocompletePredictions()){
            Log.i(TAG, prediction.getPlaceId().toString());
            Log.i(TAG, prediction.getPrimaryText(null).toString());

            predictedPlaceId = prediction.getPlaceId();
            List<Place.Field> placeFields = Arrays.asList(Place.Field.ID, Place.Field.NAME, Place.Field.LAT_LNG, Place.Field.ADDRESS);
            fetchPlaceRequest = FetchPlaceRequest.newInstance(predictedPlaceId, placeFields);
            placesClient.fetchPlace(fetchPlaceRequest).addOnSuccessListener(this).addOnFailureListener((this));
        }
    }

    // finished getting FetchPlaceRequest to get latlng from PlaceId
    private void onSuccess(FetchPlaceResponse response) {
        Place predictedPlace = response.getPlace();
        mLocations.add(predictedPlace);
        mMap.addMarker(new MarkerOptions().position(predictedPlace.getLatLng()));    //.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_YELLOW)));
        //Log.i(TAG, "-----------------------" + mLocations.get(mLocations.size()-1).getAddress().toString());
    }

    // handle failure of autocomplete prediction & FetchPlaceRequest
    public void onFailure(Exception exception) {
        Log.i(TAG, "EXCEPTION happens");
    }
}



