package com.example.myfirstmap;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.libraries.places.api.model.Place;
import com.google.android.material.tabs.TabLayout;

import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;

public class ResultsActivity extends Activity implements TabLayout.OnTabSelectedListener{
    private final String TAG = "-- ResultsActivity -- ";
    private TabLayout mtabLayout;

    public void onTabReselected(TabLayout.Tab tab) {
        Log.i(TAG, "same tab has been re-selected, doing nothing");
    }

    public void onTabSelected(TabLayout.Tab tab) {
        Log.i(TAG, "a tab has been selected");
        if (tab == mtabLayout.getTabAt(0)) {
            startActivity(new Intent(this, MapsActivity.class));
        }
    }

    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    protected void onResume() {
        Log.i(TAG, "Results activity OnResume()");
        super.onResume();
        mtabLayout.getTabAt(1).select();
    }


    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_results);

        mtabLayout = (TabLayout) findViewById(R.id.tablayout1);
        mtabLayout.addOnTabSelectedListener(this);

        EditText editText = (EditText) findViewById(R.id.editTextMulti);
        Intent intent = getIntent();

        ArrayList<Place> places = intent.getParcelableArrayListExtra("locations");
        for (Place p: places) {
            Log.i(TAG, "address = " + p.getAddress());
        }

//        for (int i = 0; i < MapsActivity.mLocations.size()-1 ; i++) {
//
//            //String message = intent.getStringExtra(new Integer(i).toString());
//            Place p = intent.getParcelableExtra(Integer.toString(i));
//            editText.append(p.getAddress()+"     ");
//            Log.i(TAG, "~~~~~~~~~~~~~~~~~~~~~~~"+p.getAddress());
//        }

    }

}
